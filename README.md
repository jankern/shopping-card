# shopping card

A PHP RESTful API to achieve a simple shopping card implementation

### API endpoints

GET product
```
/get/product/list
/get/product/id
```

POST shopping card
```
/save/shoppingcard
```
POST parameters (JSON)
```
'userdata'
'shoppingcard'
```


### Scripting types

Server side: 
* PHP ( >= 5.5.x)
* Packages: Composer, Monolog, PHPMailer

Client side:
* HTML5, SASS/CSS3, Javascript ES6, JQuery ( >= 3.2.x) 
* Nodejs/Babel/Webpack for ES6 browser transpiling, sources bundling and compressing
* Nodejs/node-sass for transpiling sass sources

## Installation

```
$ git clone git@gitlab.com:jankern/shopping-card.git
$ cd shopping-card 
$ npm install
```

To build the sources run
```
$ npm run build:dev
```

For a production built with minified sources and environment (dev/prod) configs run
```
$ npm run build:dist
```

It will be placed in shopping-card/dist

## Run the code

Start a web server to run the PHP API code. f.e. with the php build in dev web server
```
$ php -S localhost:8888 -t /path/to/shopping-card/dist
```
Open your browser with
```
http://localhost:8888/
```


