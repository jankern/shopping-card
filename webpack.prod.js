var path = require('path');
var webpack = require('webpack')
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var WriteJsonPlugin = require('write-json-webpack-plugin');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var ReplacePlugin = require('replace-webpack-plugin');

var routingSrc = path.resolve(__dirname, 'src/index.php');
var routingDist = path.resolve(__dirname, 'dist/');

var accessSrc = path.resolve(__dirname, 'src/.htaccess');
var accessDist = path.resolve(__dirname, 'dist/');

var filesSrcFolder = path.resolve(__dirname, 'src/files');
var filesDistFolder = path.resolve(__dirname, 'dist/files');

var assetsSrcFolder = path.resolve(__dirname, 'src/assets');
var assetsDistFolder = path.resolve(__dirname, 'dist/assets');

var phpVendorSrcFolder = path.resolve(__dirname, 'vendor');
var phpVendorDistFolder = path.resolve(__dirname, 'dist/files/vendor');

var webpackPlugins = [
    new CopyWebpackPlugin([{
        from: routingSrc,
        to: routingDist
    }, {
        from: accessSrc,
        to: accessDist
    }, {
        from: filesSrcFolder,
        to: filesDistFolder
    },{
        from: assetsSrcFolder,
        to: assetsDistFolder
    }, {
        from: phpVendorSrcFolder,
        to: phpVendorDistFolder
    }]),
    new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery',
        jquery: 'jquery'
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'js/vendor.bundle.min.js',
        minChunks: Infinity
    }),
    new ExtractTextPlugin({
        filename: 'css/[name].bundle.css'
    }),
    new UglifyJSPlugin({
        compress: {
            warnings: true
        },
        sourceMap:true
    }),
    new WriteJsonPlugin({
        object: {
            env: "prod"
        },
        path: path.join(__dirname, './src/files'),
        filename: 'config.json'
    }),
    new ReplacePlugin({
        entry: './src/assets/index.html',
        output: './dist/assets/index.html',
        data: {
            css: '<link type="text/css" rel="stylesheet" href="../assets/css/custom.bundle.css?v='+Date.now()+'">',
            js: '<script src="../assets/js/vendor.bundle.min.js?v='+Date.now()+'"></script>\n<script src="../assets/js/custom.bundle.min.js?v='+Date.now()+'"></script>',
            env_info: 'PROD'
        }
    })
];

module.exports = {
    entry: {
        custom: ['./src/js/index.js'],
        vendor: ['./node_modules/jquery/dist/jquery.js']
    },
    output: {
        filename: 'js/[name].bundle.min.js',
        path: path.resolve(__dirname, 'dist/assets')
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
    },
    devtool: "source-map",
    externals: {},
    plugins: webpackPlugins,
    module: {
        rules: [{
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                        loader: 'css-loader',
                        options: {
                            minimize: true
                        }
                    }]
                }),
            }, {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }]
                }),
            }, {
                test: /\.js$/,
                loader: "babel-loader",
                query: {
                    presets: ["es2015"]
                },
                include: [path.resolve(__dirname, "src/js")],
                exclude: /node_modules/
            }, {
                test: /\.jp?g$|\.png$|\.gif$/,
                loader: "url-loader"
            }, {
                test: /\.svg$|\.eot$|\.woff$|\.woff2$|\.ttf$/,
                loader: "file-loader?name=../fonts/[name].[ext]&outputPath=fonts/"
            }, {
                test: /\.js$/,
                loader: "string-replace-loader",
                query: {
                    search: 'ENVIRONMENT =',
                    replace: 'ENVIRONMENT = prod',
                    flags: 'i'
                }
            }

        ]
    },
    resolve: {
        extensions: [".js"]
    }
}