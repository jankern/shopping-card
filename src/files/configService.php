<?php 

    /*
     * Data-Klasse
     * Datenbank-Connector der Produkte abruft und bestellungen speichert 
     * Die Daten werden moment statisch ohne Datenbank zurückgeliefert. Eine Speicherung gibt es nicht
     */

    class ConfigService {
        
        // class variables
        private $config;
        // Environment variable
        private $env;
        
        // constructor
        public function ConfigService (){
            
            // Fetch Environment variable
            $jsonFile = file_get_contents('files/config.json');
            $json = json_decode($jsonFile, true);
            $this->env = $json['env'];
            
            // Fetch from Database
            $this->config = [];

            $this->config['email'] = array(
                
                'isHTML'=>true,
                'prod'=> array(
                    'isSMTP'=>false,
                    'isSMTPSecure'=>false,
                    'SMTPSecure'=>'',
                    'from'=>'bl@hodp.de',
                    'addReplyTo'=>'bl@hodp.de',
                    'host'=>'w013f1f7.kasserver.com',
                    'SMTPAuth'=>true,
                    'username'=>'m036a9d2',
                    'password'=>'sancho4mail',
                    'port'=>'25',
                    'fromName'=>'Burkhard Liepelt'
                ),
                'dev'=> array(
                    'isSMTP'=>false,
                    'isSMTPSecure'=>false,
                    'SMTPSecure'=>'',
                    'from'=>'bl@hodp.de',
                    'addReplyTo'=>'bl@hodp.de',
                    'host'=>'',
                    'SMTPAuth'=>false,
                    'username'=>'',
                    'password'=>'',
                    'port'=>'',
                    'fromName'=>'Burkhard Liepelt'    
                )
            );
        } 
        
        
        // return the entire product list
        public function getEmailConfig(){  
            
            $configEmail = [];
            
            $configEmail[$this->env] = true;
            
            foreach ($this->config['email'] as $key => $value) {
                if($key !== 'prod' && $key !== 'dev'){
                    $configEmail[$key] = $this->config['email'][$key];
                }
            }
            
            foreach ($this->config['email'][$this->env] as $key => $value) {
                $configEmail[$key] = $this->config['email'][$this->env][$key];
            }
            
            return $configEmail; 
        }
        
    }
?>