<?php 

    /*
     * Data-Klasse
     * Datenbank-Connector der Produkte abruft und bestellungen speichert 
     * Die Daten werden moment statisch ohne Datenbank zurückgeliefert. Eine Speicherung gibt es nicht
     */

    class DataService {
        
        // class variables
        public $output;
        public $productList;
        
        // constructor
        public function DataService (){
            
            // Fetch from Database
            $this->productList = [];

            $this->productList[0] = array(
                'liefernr'=> "0.12.0",
                'id'=> "1",
                'artikel'=> "Artikel 1",
                'preis'=> "114.14",
                'bild'=> "assets/img/art1.jpg",
                'beschreibung' => 'Unter einem Produkt wird in der Betriebswirtschaft eine materielles Gut oder eine immaterielle Dienstleistung verstanden, die das Ergebnis eines Input-Output-Prozesses ist.Der Begriff Produkt wird in der Wirtschaftswissenschaft häufig mit dem Begriff Erzeugnis gleichgesetzt'
            );
            $this->productList[1] = array(
                'liefernr'=> "1.33.7",
                'id'=> "2",
                'artikel'=> "Artikel 2",
                'preis'=> "10",
                'bild'=> "assets/img/art2.jpg",
                'beschreibung' => 'Unter einem Produkt wird in der Betriebswirtschaft eine materielles Gut oder eine immaterielle Dienstleistung verstanden, die das Ergebnis eines Input-Output-Prozesses ist.Der Begriff Produkt wird in der Wirtschaftswissenschaft häufig mit dem Begriff Erzeugnis gleichgesetzt'
            );
            $this->productList[2] = array(
                'liefernr'=> "1.15.0",
                'id'=> "3",
                'artikel'=> "Artikel 3",
                'preis'=> "120",
                'bild'=> "assets/img/art3.jpg",
                'beschreibung' => 'Unter einem Produkt wird in der Betriebswirtschaft eine materielles Gut oder eine immaterielle Dienstleistung verstanden, die das Ergebnis eines Input-Output-Prozesses ist.Der Begriff Produkt wird in der Wirtschaftswissenschaft häufig mit dem Begriff Erzeugnis gleichgesetzt'
            );
            $this->productList[3] = array(
                'liefernr'=> "1.16.6",
                'id'=> "4",
                'artikel'=> "Artikel 4",
                'preis'=> "23.99",
                'bild'=> "assets/img/art4.jpg",
                'beschreibung' => 'Unter einem Produkt wird in der Betriebswirtschaft eine materielles Gut oder eine immaterielle Dienstleistung verstanden, die das Ergebnis eines Input-Output-Prozesses ist.Der Begriff Produkt wird in der Wirtschaftswissenschaft häufig mit dem Begriff Erzeugnis gleichgesetzt'
            );
        } 
        
        // return the entire product list
        public function getProductList($returnType){
            
            if($returnType == "JSON") {
                $this->productList = $this->_convertToJson($this->productList);
            }       
            
            return $this->productList; 
        }
        
        // return the entire product list
        public function getProductSingle($returnType, $id){
            
            $product = $this->_searchProduct($id);
            
            if($returnType == "JSON") {
                $product = $this->_convertToJson($product);
            }       
            
            return $product; 
        }
        
        public function saveOrder(){
            
            // Save to Database
    
        }
        
        private function _convertToJson($list){
            return json_encode($list);
        }
        
        private function _convertToArray($jsonString){
            
        }
        
        private function _searchProduct($id){
            foreach ($this->productList as $list){
                if ($list['id'] == $id)
                    return $list;
            }
        }
        
    }
?>