<?php

    /*
     * Order-Klasse
     * Bestellungen werden erstellt, Rückgabe ist ein JSON-String, der zum browser zurückgesendet wird 
     * Der Datenbankservice wird aufgerufen (nicht implementiert) , um die Bestellung zu speichern
     * Der Emailservice zum Versenden der Bestellung wird ausgeführt
     */

    class OrderService {
        
        // class variables
        
        // constructor
        public function OrderService (){
            
            $this->dataService = new DataService();
            $this->emailService = new EmailService();
            
        } 
        
        // return the entire product list
        public function createOrder(){
            
            if(isset($_POST["shoppingcard"])){
                // create Order from json
                $shoppingCard = json_decode($_POST["shoppingcard"], true);
                $userData = json_decode($_POST["userdata"], true);
                
                // create order number
                $shoppingCard["ordernr"] = $this->_createOrderNumber(8);
            
                // save order to data base
                $data = $this->dataService->saveOrder($shoppingCard);

                // send email with order data
                $mailIsSent = $this->emailService->sendMail($shoppingCard, $userData);
                
                // return orer number to browser
                return '{"mailinfo":"'.$mailIsSent.'", "ordernr": "'.$shoppingCard['ordernr'].'"}';
            }
            
        }
        
        private function _convertToArray($jsonString){
            return json_decode($jsonString);
        }
        
        private function _createOrderNumber($length) {
            $key = '';
            $keys = array_merge(range(0, 9), range('A', 'Z'));

            for ($i = 0; $i < $length; $i++) {
                $key .= $keys[array_rand($keys)];
            }

            return $key;
        }
        
    }
?>