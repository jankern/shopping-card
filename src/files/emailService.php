<?php 

    /*
     * Email-Klasse
     * Erstellung und Versendung des Emailinhalts mittels phpmailer lib
     */

    date_default_timezone_set('Etc/UTC');
    require __DIR__ . '/vendor/autoload.php';

    class EmailService{
        
        private $config = [];
        
        // constructor
        public function EmailService (){
            
            $this->configService = new ConfigService();
            $this->config['email'] = $this->configService->getEmailConfig();
            
        } 
        
        // return the entire product list
        public function sendMail($shoppingCard, $userData){
            
            $receiver = $userData['email'];
            $subject = "Ihre Bestellung von XY-Shop";
            $message = "<h2>Sehr geehrter Kunde,</h2><p>anbei erhalten Sie die Details zu Ihrer Bestellung</p><p>Ihre Bestellnummer: ".$shoppingCard['ordernr']."</p><br><br>";
           
            
            // generated body
            $table = "<table style=\"border:none\" cellpadding=\"5\"><tr><td width=\"80\"><b>Art.Nr.</b></td><td width=\"80\"><b>Artikel</b></td><td width=\"60\" align=\"right\"><b>Preis €</b></td></tr>";
            
            if($shoppingCard['list']){
                for($i = 0; $i < sizeof($shoppingCard['list']); $i++){
                    $table .= "<tr><td>".$shoppingCard['list'][$i]["liefernr"]."</td>";
                    $table .= "<td>".$shoppingCard['list'][$i]["artikel"]."</td>";
                    $table .= "<td align=\"right\">".$shoppingCard['list'][$i]["preis"]."</td></tr>";
                }
            }
            
            if($shoppingCard['totals']){
                $table .= "<tr><td></td><td>MwSt:</td><td align=\"right\">".$shoppingCard['totals']["vat"]."</td></tr>";
                // $table .= "<tr><td></td><td>Netto:</td><td>".$shoppingCard['totals']["netto"]."</td></tr>";
                $table .= "<tr><td></td><td>Summe:</td><td align=\"right\"><b>".$shoppingCard['totals']["brutto"]."</b></td></tr>";                
            }

            $table .= "</table>";
            
            $message .= $table;
            $message .= "<p>Mit freundlichen Grüßen,</p><p>Ihr XY-Shop</p>";    
            
            //PHPMailer Object
            $mail = new PHPMailer;
            
            $mail->CharSet = 'UTF-8';
            if($this->config['email']['isSMTP']){
                $mail->isSMTP();
            }
            
            $mail->Host = $this->config['email']['host'];
            $mail->SMTPAuth = $this->config['email']['SMTPAuth'];
            $mail->Username = $this->config['email']['username'];
            $mail->Password = $this->config['email']['password'];
            if($this->config['email']['isSMTPSecure']){
                $mail->SMTPSecure = $this->config['email']['SMTPSecure'];
            }
            $mail->Port= $this->config['email']['port'];
            
            //From email address and name
            $mail->From = $this->config['email']['from'];
            $mail->FromName = $this->config['email']['fromName'];
            
            //Address to which recipient will reply
            $mail->addReplyTo($this->config['email']['addReplyTo'], "Reply");

            //CC and BCC
            // $mail->addCC("cc@example.com");
            // $mail->addBCC("bcc@example.com");

            //To address and name
            // $mail->addAddress("recepient1@example.com", "Recepient Name");
            $mail->addAddress($userData['email']); //Recipient name is optional
            
            //Send HTML or Plain Text email
            $mail->isHTML($this->config['email']['isHTML']);
            
            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 0;
            //Ask for HTML-friendly debug output
            $mail->Debugoutput = 'html';

            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->AltBody = "This is the plain text version of the email content";

            if(!$mail->send()) {
                return "Mailer Error: " . $mail->ErrorInfo;
            } 
            else {
                return "Wir haben Ihnen eine Bestätigungs-Email zugesendet.";
            }
            
        }
        
    }
?>