<?php 
    
    /*
     * Router-Klasse
     * Die HTTP-Pfade werden interpretiert 
     * Return-Wert ist ein Array ($result), dass Steuer-Flags zurückgibt
     */

    class RouterService {
        
        public function RouterService () {
            
        }
        
        public function parsePath($path){
            
            $result = [];
            
            if(preg_match('/\/get\/product\/(?![0-9a-z-.])/', $path)){
                $result['method'] = "unavailable"; 
                
            }else if (preg_match('/\/get\/product\/(?!list)/', $path)){
                $result['method'] = "getProduct";
                $result['type'] = "single"; 
                
                // Extract id from URL pathinfo
                $pathNodes = explode("/", $path);
                $result['id'] = $pathNodes[3];
                
                if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
                     $result['method'] = "unavailable";
                }
                
            }else if(preg_match('/\/get\/product\/[0-9a-z-.]+/', $path)){
                $result['method'] = "getProduct";
                $result['type'] = "list";
                
                if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
                     $result['method'] = "unavailable";
                }
                
            }else if (preg_match('/get\/shoppingcard/',$path)){
                $result['method'] = "getShoppingCard";
                // TODO - needs to be defined
                
                if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
                     $result['method'] = "unavailable";
                }
                
            }else if (preg_match('/save\/shoppingcard/',$path)){
                $result['method'] = "saveShoppingCard";
                
                if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
                     $result['method'] = "unavailable";
                }
                
            }else if (preg_match('/delete\/shoppingcard/',$path)){
                $result['method'] = "deleteShoppingCard";
                // TODO - needs to be defined
                
                if ($_SERVER['REQUEST_METHOD'] !== 'DELETE') {
                     $result['method'] = "unavailable";
                }

            }else if(preg_match('/^\/$/', $path)){
                $result['method'] = "index";
                
            }else{
                $result['method'] = "unavailable"; 
            }
            
            return $result;
            
        }
        
    }

?>