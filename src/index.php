<?php

    /*
     *  Die Index-Datei ist Startdatei aller Scripts
     *  Hier werden 
     *  1. Alle Klassen importiert, instanziiert (Router, Datenbank (wobei es momentan keine gibt), Order, Email)
     *  2. Die Routing-Regeln abgefragt, wie welcher Browser-Request behandelt werden soll (getProduct, getProductSingle, saveShoppingCard etc.)
     *  3. Der Response zum Browser zurückgeschickt
     */


    // Imports
    include 'files/routerService.php';
    include 'files/dataService.php';
    include 'files/orderService.php';
    include 'files/emailService.php';
    include 'files/configService.php';

    // Service Classes
    $routerService = new RouterService();
    $dataService = new DataService();
    $orderService = new OrderService();

    // path parsing to decide which data to fetch and which action to process
    $routing = $routerService->parsePath($_SERVER['REQUEST_URI']);
        
    // Handling request types
    // REST API actions
    switch ($routing["method"]){
        
        // Get product data and send to client (GET)
        case "getProduct":

            header('Content-type:application/json;charset=utf-8');
            
            if($routing["type"] == 'single'){
                $data = $dataService->getProductSingle('JSON', $routing["id"]);
                echo $data;
            }
            if($routing["type"] == 'list'){
                $data = $dataService->getProductList('JSON');
                echo $data;
            }
            break;
            
        case "getShoppingCard":
            // TODO - needs to be defined
            break;
            
        // Receive shopping card details from client (POST)
        case "saveShoppingCard":
            $data = $orderService->createOrder();
            header('Content-type:application/json;charset=utf-8');
            echo $data;
            break;
            
        // Receive shopping card details from client to delete (DELETE)
        case "deleteShoppingCard":
            // TODO - needs to be defined
            break;
            
        // Get product list for index page
        case "index":
            
            $indexOutput = "";
            
            // Retrieve product list data
            $data = $dataService->getProductList('Array');
            
            // Template import to add data grid
            include_once "assets/index.html";
            break;   
            
        case "unavailable":
            http_response_code(404); 
            break;
            
        case "default":
            break;
    }

?>