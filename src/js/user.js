/*
 *  Class User
 */
export
default class User {

    constructor() {}
    
    /*
     *  Method to validate user input fields
     *  @param   String fieldType
    **/

    validate(fieldType) {

        $('.message').css({
            'display': 'none'
        });
        $('input[name=email]').css({
            'borderColor': '#C5CAD1'
        });

        switch (fieldType) {
            case "email":

                // Validate email field if filled
                if ($('input[name=email]').val() === "" || $('input[name=email]').val() === undefined) {
                    $('.message').css({
                        'display': 'block'
                    });
                    $('input[name=email]').css({
                        'borderColor': 'red'
                    });
                    $('.message').html('<i class="icon-warning"></i> Sie müssen eine Email angeben!');
                    // If not cancel validation and stop the script
                    return false;
                }

                // Validate email field if format correct
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('input[name=email]').val()) === false) {
                    $('.message').css({
                        'display': 'block'
                    });
                    $('input[name=email]').css({
                        'borderColor': 'red'
                    });
                    $('.message').html('<i class="icon-warning"></i> Sie müssen eine gültige Email-Adresse angeben!');
                    // If not cancel validation and stop the script
                    return false;
                }

                break;
        }

        return true;
    }

}