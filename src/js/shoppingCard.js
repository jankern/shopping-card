/*
 *  Class shoppingCard
 */
export
default class shoppingCard {

    constructor() {
        this.sc = {
            list: [],
            totals: {},
            general: {
                vat: 19
            }
        }
    }
    
    /*
     *  Method to push one item out of the product list to the shopping card array
     *  @param      Array productList   
     *  @param      String id           Product id
     **/

    pushProductToShoppingCard(productList, id) {
        if (id) {
            for (let i = 0; i < productList.length; i++) {
                if (productList[i].id == id) {
                    // Add a copy and no reference to the shopping card object
                    this.sc.list.push(jQuery.extend(true, {}, productList[i]));

                    // Create a shopping card id to have unique elements in shopping card array
                    this.sc.list[this.sc.list.length - 1].sid = "";
                    this.sc.list[this.sc.list.length - 1].sid = this.generateShoppingCardId();
                }
            }

            this.prepareShoppingCard();
        }
    }

    /*  
     *  Method to push a single product to the shopping card array
     *  @param      Object singleProduct
     **/

    pushSingleProductToShoppingCard(singleProduct) {

        // Add single product to shopping card
        this.sc.list.push(jQuery.extend(true, {}, singleProduct));

        // Create a shopping card id to have unique elements in shopping card array
        this.sc.list[this.sc.list.length - 1].sid = "";
        this.sc.list[this.sc.list.length - 1].sid = this.generateShoppingCardId();

        this.prepareShoppingCard();
    }
    
    /*
     *  Method to prepare the shopping card object and HTML
     **/

    prepareShoppingCard() {

        let tableStr = '';
        let tableSumStr = '';

        // Insert an empty table into the DOM in case product list is empty
        if (this.sc.list.length <= 1) {
            $('.shopping-card-list').append('<table class="shopping-card-table"><tr><td></td><td>Artikel Nr</td><td>Artikel</td><td>Preis</td><td></td></tr></table><table class="shopping-card-amount-table"></table');

            $('#shopping-card-checkout').prop("disabled", "");
            $('#shopping-card-send').prop("disabled", "");
        }

        // Create a table row containing the created product item
        tableStr += '<tr><td><img src="' + this.sc.list[this.sc.list.length - 1].bild + '" height="40" width="40"></td><td>' + this.sc.list[this.sc.list.length - 1].liefernr + '</td><td>' + this.sc.list[this.sc.list.length - 1].artikel + '</td><td class="right">' + this.sc.list[this.sc.list.length - 1].preis + '</td><td class="right" width=200"><button id="' + this.sc.list[this.sc.list.length - 1].sid + '">Entfernen</button></td></tr>';

        // Insert the table row into the table
        $('.shopping-card-list table.shopping-card-table').append(tableStr);

        // Add a click event to the remove button
        $('.shopping-card-list #' + this.sc.list[this.sc.list.length - 1].sid).on('click', (event) => {
            this.removeProductFromShoppingCard($(event.currentTarget).attr('id'));
        });

        // Call the shopping card calculation
        tableSumStr = this.calculateShoppingCardAmount();

        // Insert the shopping card calculation HTML
        $('.shopping-card table.shopping-card-amount-table').html(tableSumStr);

    }
    
    /*
     *  Method to calculate the amount of all shopping card items and creating the HTML
     *  @param      Array data     product list (json string)
     **/

    calculateShoppingCardAmount() {

        let bruttoAmount = 0;
        let nettoAmount = 0;
        let vatAmount = 0;

        for (let i = 0; i < this.sc.list.length; i++) {
            bruttoAmount = bruttoAmount + parseFloat(this.sc.list[i].preis);
        }

        // Round all decimals to two digits 
        bruttoAmount = parseFloat(Math.round(bruttoAmount * 100) / 100).toFixed(2);
        vatAmount = parseFloat(Math.round((bruttoAmount * (this.sc.general.vat / 100)) * 100) / 100).toFixed(2);
        nettoAmount = parseFloat(Math.round((bruttoAmount - vatAmount) * 100) / 100).toFixed(2);

        // Adding amount values to the global shopping card object
        this.sc.totals = {
            brutto: bruttoAmount,
            vat: vatAmount,
            netto: nettoAmount
        };
        console.log('Warenkorb totals:');
        console.log(this.sc.totals);

        // Add amount calculation to shopping card in a separate table
        let tableSumStr = '<tr><td colspan="2">MwSt(%)</td><td>' + this.sc.general.vat + '</td><td class="right">' + vatAmount + '</td><td width=200"></td></tr>' +
            '<tr><td colspan="3">Summe:</td><td class="right">' + bruttoAmount + '</td><td width=200"></td></tr>';

        return tableSumStr;
    }

    /*
     *  Callback method for product list creation while app startup
     *  @param      Array data     product list (json string)
     **/
    
    removeProductFromShoppingCard(sid) {
        for (let i = 0; i < this.sc.list.length; i++) {
            if (this.sc.list[i].sid == sid) {
                this.sc.list.splice(i, 1);
            }
        }

        // Remove event listener to avoid memory leaks
        $("#" + sid).off('click');

        // Remove the element from DOM
        $("#" + sid).parent().parent().remove();

        // Remove and disabled shopping card functions when card is empty
        if (this.sc.list.length <= 0) {
            $('.shopping-card-list').html('');
            $('#shopping-card-checkout').prop("disabled", "true");
            $('#shopping-card-send').prop("disabled", "true");
        } else {
            // Otherwise recalculate the amount of the shopping card items
            $('.shopping-card table.shopping-card-amount-table').html(this.calculateShoppingCardAmount());
        }
    }
    
    /*
     *  Callback method for product list creation while app startup
     *  @param      Array data     product list (json string)
     **/

    generateShoppingCardId() {
        let char = "";
        let range = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (let i = 0; i < 5; i++) {
            char += range.charAt(Math.floor(Math.random() * range.length));
        }
        return char;
    }

}