/*
 *  Class Navigation
 */

import Remote from './remote';
import Product from './product';
import ShoppingCard from './shoppingCard';
import User from './user';

export
default class Navigation {

    constructor() {

        // Instantiate all classes
        this.remote = new Remote();
        this.product = new Product();
        this.shoppingCard = new ShoppingCard();
        this.user = new User();

        this.prepareProductList = this.prepareProductList.bind(this);
        this.initShoppingCardEventHandlers = this.initShoppingCardEventHandlers.bind(this);
    }

    /*
     *  Method is routing all incoming url hash IDs and calls the related functions
     **/

    route() {

        let locationHash = location.hash || "";

        // Condition matches if has value exists
        if (locationHash !== "") {

            let urlProductId = location.hash.split(':');

            // Condition matches if url hash contains product id
            if (urlProductId.length > 1) {

                // If a single product exists, existing event will be removed to avoid memory leaks
                if (this.product.p.single !== "") {
                    $('.product-single #single-' + this.product.p.single).off('click');
                }

                // Registrate single product in global variable
                this.product.p.single = urlProductId[1];

                // Call for a single product
                this.remote.productSingle(this.product.p.single, (data) => {
                    console.log('Einzelprodukt gerufen:')
                    console.log(data);

                    // Add single product to screen
                    let singleProduct = this.product.prepareSingleProductForHTML(data)
                    $('.product-single').html(singleProduct);

                    // Add a click event to single product button
                    $('.product-single #single-' + this.product.p.single).on('click', (event) => {
                        this.shoppingCard.pushSingleProductToShoppingCard(data);
                        // After clicking the single product button, screen will return to product list
                        if (this.product.p.list.length <= 0) {
                            // Call product list if empty
                            this.remote.productList(this.prepareProductList);
                        } else {
                            this.screen('showList');
                        }
                    });

                    // Add a click event to return to product list button
                    $('.product-single #back-to-product-list-' + this.product.p.single).on('click', (event) => {
                        // After clicking the return to product list button, screen will return to product list
                        if (this.product.p.list.length <= 0) {
                            // Call product list if empty
                            this.remote.productList(this.prepareProductList);
                        } else {
                            this.screen('showList');
                        }
                    });

                    // If shopping card is empty, disable it
                    if (this.shoppingCard.sc.list.length <= 0) {
                        $('#shopping-card-checkout').prop("disabled", "true");
                        $('#shopping-card-send').prop("disabled", "true");
                    }

                });

                // Go to product single view
                this.screen('showSingle');
            }
        } else {
            // Call product list
            this.remote.productList(this.prepareProductList);
        }
    }

    /*
     *  Method displays and hides screen sections
     *  @param   String status
     **/

    screen(status) {
        switch (status) {
            case "showList":
                console.log("Screen type: showProductList");
                $('.show-list').css({
                    'display': 'block'
                });
                $('.show-single').css({
                    'display': 'none'
                });
                $('.show-shopping-card-single').css({
                    'display': 'block'
                });
                $('.hide-shopping-card-single').css({
                    'display': 'none'
                });
                break;

            case "showSingle":
                console.log("Screen type: showSingleProduct");
                $('.show-list').css({
                    'display': 'none'
                });
                $('.show-single').css({
                    'display': 'block'
                });
                $('.show-shopping-card-single').css({
                    'display': 'block'
                });
                $('.hide-shopping-card-single').css({
                    'display': 'none'
                });
                break;

            case "showShoppingCardOnly":
                console.log("Screen type: showShoppingCardOnly");
                $('.show-list').css({
                    'display': 'none'
                });
                $('.show-single').css({
                    'display': 'none'
                });
                $('.show-shopping-card-single').css({
                    'display': 'none'
                });
                $('.hide-shopping-card-single').css({
                    'display': 'block'
                });
                break;
        }
    }

    /*
     *  Callback method for product list creation while app startup
     *  @param      String id     URL hash id
     **/

    changeLocationHash( /*String*/ id) {
        window.location.hash = id;
        this.route();
    }

    /*
     *  Callback method for product list creation while app startup
     *  @param      Array data     product list (json string)
     **/

    prepareProductList( /*Array*/ data) {
        this.product.p.list = data;
        console.log('Produktliste gerufen:');
        console.log(this.product.p.list);

        if (this.product.p.list.length > 0) {
            // Attach list item HTML to the existing list
            $('.product-list').append(this.product.prepareProductListForHTML(this.product.p.list));
            // All product buttons get a click event
            $('.product-list').on('click', '.add-to-card', (event) => {
                this.shoppingCard.pushProductToShoppingCard(this.product.p.list, $(event.currentTarget).attr('id'));
            });
            // All single product buttons get a click event
            $('.product-list').on('click', '.product-details', (event) => {
                this.changeLocationHash($(event.currentTarget).attr('id'));
            });
        }

        if (this.shoppingCard.sc.list.length <= 0) {
            $('#shopping-card-checkout').prop("disabled", "true");
            $('#shopping-card-send').prop("disabled", "true");
        }

        this.screen('showList');
    }

    prepareShoppingCardSaveResponse(data) {
        $('.shopping-card').html('<p>Vielen Dank für Ihren Einkauf. ' + data.mailinfo + ' Ihre Bestellnummer lautet: <b>' + data.ordernr + '</b></p>');
    }

    /*
     *  Initialaising Event handler for static shoppingCard buttons
     **/

    initShoppingCardEventHandlers() {
        // Event handler for shopping card checkout
        $('#shopping-card-checkout').on('click', (event) => {
            this.screen("showShoppingCardOnly");
        });

        // Event handler for return to product list
        $('#shopping-card-back-to-list').on('click', (event) => {
            this.screen("showList");
        });

        // Event handler for send/save shopping card
        $('form').submit((event) => {
            event.preventDefault();

            // Validate user input fields
            let isValid = this.user.validate('email');

            // If it's not valid terminate the save action
            if (!isValid) {
                return;
            }

            // Assigment of data fields to the POST object to be sent
            let formData = {
                'userdata': JSON.stringify({
                    "email": $('input[name=email]').val()
                }),
                'shoppingcard': JSON.stringify({
                    "list": this.shoppingCard.sc.list,
                    "totals": this.shoppingCard.sc.totals
                })
            };

            // Call POST request method and insert response values into the screen
            this.remote.saveShoppingCard(formData, this.prepareShoppingCardSaveResponse);
        });
    }

}