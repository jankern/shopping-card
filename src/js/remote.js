/*
 *  Class Remote
 */
export
default class Remote {

    constructor() {}

    /*
     *  Method calls list data from GET endpoint
     *  @param   Function callBack
    **/
    
    productList(callBack) {
        $.get("/get/product/list", (data, status) => {
            callBack(data);
        });
    }

    /*
     *  Method calls single data from GET endpoint
     *  @param   String id              product id
     *  @param   Function callBack      Function to be processed after response arrived
    **/
    
    productSingle(id, callBack) {
        $.get("/get/product/" + id, (data, status) => {
            callBack(data);
        });
    }
    
    /*
     *  Method sends shopping card data to POST endpoint
     *  @param   object data            post data
     *  @param   Function callBack
    **/

    saveShoppingCard(data, callBack) {

        $.ajax({
            type: 'POST',
            url: '/save/shoppingcard/',
            data: data,
            dataType: 'json',
            encode: true
        }).done((data) => {
            console.log('zurück in done '+data);
            callBack(data);
        });
    }
}