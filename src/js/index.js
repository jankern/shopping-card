/*
 *  ENVIRONMENT = 
 */

// Webpack imports
import '../scss/styles.scss';
import $ from 'jquery';

// Class and Function Imports
import Navigation from './navigation';

    // Class intialisation
let navigation = new Navigation();


// JQuery $(document).ready function to start generating the screen
$(function() {
    
    navigation.route();
    navigation.initShoppingCardEventHandlers();
    console.log('Rendered');
    
});