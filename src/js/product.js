/*
 *  Class Product
 */
export
default class Product {

    constructor() {
        
        // Product object
        this.p = {
            list: [],
            single: ""
        };
    }
    
    /*
     *  Method prpepares the HTML to insert the product list into the DOM
     *  @param      Array productList
     *  @return     String tableStr
    **/

    prepareProductListForHTML(productList) {
        let tableStr = '<h2>Produktliste</h2><table><tr><td></td><td>Liefer-Nr.</td><td>Artikel</td><td>Preis</td><td></td></tr>';
        for (var i = 0; i < productList.length; i++) {
            tableStr += '<tr><td><img src="' + productList[i].bild + '" height="40" width="40"></td><td>' + productList[i].liefernr + '</td><td>' + productList[i].artikel +
                '</td><td class="right">' + productList[i].preis + '</td><td class="right"><button class="product-details" id="product:' + productList[i].id + '">Details</button>' +
                '<button class="add-to-card" id="' + productList[i].id + '">In den Warenkorb</button></td></tr>';
        }
        tableStr += '</table>';
        
        return tableStr;
    }
    
    /*
     *  Method prpepares the HTML to insert a single product into the DOM
     *  @param      Object singleProduct
     *  @return     String divStr
    **/

    prepareSingleProductForHTML(singleProduct) {
        let divStr = '<div class="col-img"><img src="' + singleProduct.bild + '" height="330" width="330"></div>';
        divStr += '<div class="col-text"><p>' + singleProduct.artikel + ', ' + singleProduct.liefernr + '</p><p>' + singleProduct.beschreibung + '</p></div>';
        divStr += '<div class="col-nav"><button id="back-to-product-list-' + singleProduct.id + '">Zurück zur Produktliste</button><button id="single-' + singleProduct.id + '">In den Warenkorb</button></div>';
        
        return divStr;
    }

}